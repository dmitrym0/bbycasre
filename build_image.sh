#!/bin/bash



# if branch is empty we're running in gitlab ci, they already have the branch name set.
HASH=`git log --pretty=format:%h -1`
BRANCH_NAME=`git branch | grep -e "^*" | cut -d' ' -f 2|sed -e 's/\\\(HEAD-//g'`
   
CONTAINER_NAME="registry.gitlab.com/dmitrym0/bbycasre"
FULL_NAME="${CONTAINER_NAME}:${BRANCH_NAME}-${HASH}"
echo "Building new container: ${FULL_NAME}"

docker build . -t ${FULL_NAME} || exit "Container build failed"
