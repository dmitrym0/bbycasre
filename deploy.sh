#!/bin/bash

if [[ -z "$1" || -z "$2" ]];
   then
    echo "Pleas supply args: "
    echo "$0 ENV IMAGE"
    echo "Example: $0 bbprod master-a4fbff9"
    exit 0
fi

ENV=$1
IMAGE=$2

perl -i -pe "BEGIN { undef $/; } s/${ENV}:\n.*image.*/${ENV}:\n    image: registry\.gitlab\.com\/dmitrym0\/bbycasre\:$IMAGE/mg" docker-compose.yml || exit 1

docker-compose up --no-deps -d
