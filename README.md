# bbcaSRE Deployment Pipeline

# Introduction

For this application, I decided to create a simple, but flexible build and deployment pipeline:

-   Builds can be invoked manually (via `build_image.sh` script) or automatically via GitLab-CI
-   Builds are deployed manually via the `deploy.sh` script
-   The artefact produced during the build is a docker container. I believe containers are superior method to deploy web applications.
-   Containers are deployed to a low-cost cloud provider (vultr) via `docker-compose`.

#  Description

One of hallmarks of a great build pipeline is flexibility. In my experience is very important to be able to build the application both locally and remotely. 
Towards this end, I'm providing both a cloud-based build solution (GitLab-CI) as well as a script that can be used by developers localy.

The output of the build process is a docker container. In my opinion it's the superior of way of distributing applications. It allows for composability, isolation at runtime
and a host of other benefits. There are some drawbacks, in particular size of the resultant binary object, but I still think it's worth it.

Deployments are initiated manually by running `deploy.sh` script. It would be possible to add the script to the build pipeline to invoke deployments based on business rules (eg 
deploy to "dev" as soon as a container is built successfully). 

`deploy.sh` wraps existing docker functionality for transport and runtime management with docker-compose. I'm using nginx-proxy to multiplex requests to each "environment" based
on the requested hostname.

# Testing

Application is deployed in the cloud, to test please visit:

-   prod: <http://bbsre-prod.duckdns.org>
-   dev: <http://bbsre-dev.duckdns.org>
-   dr: <http://bbsre-dr.duckdns.org>
-   test: <http://bbsre-test.duckdns.org>

#  Building

Application will be built automatically by Gitlab-CI (see `.gitlab-ci.yml`) upon each commit. See <https://gitlab.com/dmitrym0/bbycasre/container_registry> for containers.

To build the application manually invoke `build_image.sh`. It will produce a local docker image that then needs to be pushed to the registry.

# Deploying


**Note**: before deploying, ensure you have access to the remote machine (see "Provisioning and Accessing" below).

Use `deploy.sh` script to deploy to specific environment.

There are 4 environment defined:

-   bbprod
-   bbtest
-   bbdev
-   bbdr

Build procedure will produce images of the name registry.gitlab.com/dmitrym0/bbycasre:master-1a9fd675

Deploy script requires two parameters:

-   environment (eg: `bbprod`)
-   image tag (eg: `master-1a9fd675`, from above)

Running 

    eval $(docker-machine env bb)       # gain access to the remote machine
    ./deploy.sh bbprod master-1a9fd675

will update the bbprod image to master-1a9fd675 and restart the service

#  Provisioning and Accessing remote VM

Remote VM (66.42.69.70) was provisioned using docker-machines. To enable deployments you *must* import docker-machine configuration. 

1.  Install <https://github.com/bhurlow/machine-share>
2.  Run "machine-import bb.zip"
3.  Install a relatively new version (tested with Docker version 18.09.2, build 6247962, MacOS Mojave 10.14)

To verify successful installation & import, run:

    > eval $(docker-machine env bb)
    > docker ps
  
    CONTAINER ID        IMAGE                                                   COMMAND                  CREATED             STATUS              PORTS                   NAMES
    2e8914cde815        jwilder/nginx-proxy                                     "/app/docker-entrypo…"   2 hours ago         Up 2 hours          0.0.0.0:80->80/tcp      bbycasre_nginx-proxy_1
    7096a3df8a4e        registry.gitlab.com/dmitrym0/bbycasre:master-1a9fd675   "docker-entrypoint.s…"   2 hours ago         Up 2 hours          0.0.0.0:32802->80/tcp   bbycasre_bbdev_1
    4c9ab2acbad6        registry.gitlab.com/dmitrym0/bbycasre:master-4848f243   "docker-entrypoint.s…"   2 hours ago         Up 2 hours          0.0.0.0:32803->80/tcp   bbycasre_bbprod_1
    7f013a213694        registry.gitlab.com/dmitrym0/bbycasre:master-4848f243   "docker-entrypoint.s…"   2 hours ago         Up 2 hours          0.0.0.0:32801->80/tcp   bbycasre_bbdr_1 
    2bd894182b93        registry.gitlab.com/dmitrym0/bbycasre:master-4848f243   "docker-entrypoint.s…"   2 hours ago         Up 2 hours          0.0.0.0:32800->80/tcp   bbycasre_bbtest_1
