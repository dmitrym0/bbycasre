FROM node:10

# from https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

WORKDIR /usr/src/app

COPY . ./

RUN npm install express ejs

CMD [ "node", "bestbuy.ca.js" ]

